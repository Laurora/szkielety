from django.urls import path
from . import views

#URL Configuration
urlpatterns = [
    path('login/', views.login),
    path('register/', views.register),
    path('home/', views.homePage)
]