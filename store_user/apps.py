from django.apps import AppConfig


class Store_userConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'store_user'
