from django.shortcuts import render
from django.http import HttpResponse

def login(request):
    return render(request, 'login.html', {'name': 'Kasia'})

def register(request):
    return render(request, 'register.html', {'name': 'Kasia'})

def homePage(request):
    return render(request, 'homePage.html', {'name': 'Kasia'})
