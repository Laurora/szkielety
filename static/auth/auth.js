var u = JSON.parse(localStorage.getItem("u"))

if (!u) {
    localStorage.setItem("u", JSON.stringify([{
        "_id": "dk4093kdsadsa",
        "email": "mbow@gmail.com",
        "password": "vbn54"
    }]))
}

class Db {
    static users = JSON.parse(localStorage.getItem("u"))
}

function login() {
    var errorLog =  document.getElementById("errorLog");
    var email = document.getElementById("form2Example11").value;
    var password = document.getElementById("form2Example22").value;
    for(var user of Db.users) {
        if (user.email === email && user.password === password) {
            window.location.replace("http://127.0.0.1:8000/store/products/");
            errorLog.innerHTML = "";
            return;
        } 
    }
    errorLog.innerHTML = "Incorrect email or password has been entered";
}  

function toRegister() {
    window.location.replace("http://127.0.0.1:8000/store/register/");
}


function register() {
    var firstName = document.getElementById("firstName").value;
    var lastName = document.getElementById("lastName").value;
    var birthdayDate = document.getElementById("birthdayDate").value;
    var bronzeMembership = document.getElementById("bronzeMembership").checked;
    var silverMembership = document.getElementById("silverMembership").checked;
    var goldMembership = document.getElementById("goldMembership").checked;
    var emailAddress = document.getElementById("emailAddress").value;
    var password = document.getElementById("password").value;

    if (emailAddress === '' || password === '') {
        errorLog.innerHTML = "You must enter at least your email and password";
    } else {
        var membership;

        if (bronzeMembership) {
            membership = "bronzeMembership"
        } else if (silverMembership) {
            membership = "silverMembership"
        } else if (goldMembership) {
            membership = "goldMembership"
        }

        Db.users.push({ 
            "firstName": firstName,
            "lastName": lastName,
            "birthdayDate": birthdayDate,
            "membership": membership,
            "email": emailAddress, 
            "password": password,
        })

        localStorage.setItem("u", JSON.stringify(Db.users))
        
        errorLog.innerHTML = ""
        window.location.replace("http://127.0.0.1:8000/store/login/");
    }
}
